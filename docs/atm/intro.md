---
sidebar_position: 1
---

# ATM Intro

Include the documentation for the assigned perspective of the ATM transaction.

Here are reported the links to the different perspectives:

* [ATM Transaction from a Bank's Perspective](docs/atm/atm_transaction/bank.md)
* [ATM Transaction from a Customer's Perspective](docs/atm/atm_transaction/customer.md)
* [ATM Transaction from an ATM's Perspective](docs/atm/atm_transaction/atm.md)
* [ATM Transaction from a Transaction's Perspective](docs/atm/atm_transaction/transaction.md)
* [ATM Transaction from a Debit card's Perspective](docs/atm/atm_transaction/debit-card.md)

You should include the documentation for the assigned perspective in the corresponding file.
It can be found in the `docs/atm/atm_transaction` directory.
