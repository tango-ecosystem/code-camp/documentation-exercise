# Documentation exercise

This website is built using [Docusaurus](https://docusaurus.io/), a modern static website generator.

TODO: Fill in the description.

### Installation

```bash
yarn
```

### Local Development

```bash
yarn start
```

This command starts a local development server and opens up a browser window. Most changes are reflected live without having to restart the server.

### Build

```bash
yarn build
```

This command generates static content into the `build` directory and can be served using any static contents hosting service.

### Structure

The project is structured as follows:
    
```plaintext
    ├── CHANGELOG.md
    ├── docs
    │   ├── atm
    │   │   ├── atm_transaction
    │   │   │   └── TODO include the file of the assigned part
    │   │   └── intro.md
    │   └── tutorial
    │       ├── tutorial-basics
    │       ├── tutorial-extras
    │       └── intro.md
    ├── README.md
    ├── sidebars.js
    ├── src
    │   ├── components
    │   ├── css
    │   └── pages
    └── static
        └── img
```
